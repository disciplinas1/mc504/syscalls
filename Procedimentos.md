# Procedimentos - Projeto05 MC504

## Kernel Syscall

- Added arch/x86/entry/syscalls/syscall_64.tbl

        448 common  getuserweight   sys_getuserweight
        449 common  setuserweight   sys_setuserweight

- Added include/linux/syscalls.h

        /* User weighted round robin */
        asmlinkage int sys_getuserweight(int uid);
        asmlinkage int sys_setuserweight(int uid, int weight);

- Added include/uapi/asm-generic/unistd.h

        #define __NR_getuserweight 448
        __SYSCALL(__NR_getuserweight, sys_getuserweight)
        #define __NR_setuserweight 449
        __SYSCALL(__NR_setuserweight, sys_setuserweight)

        #undef __NR_syscalls
        #define __NR_syscalls 450

- Added kernel/sys_ni.c

        /* User weighted round robin */
        COND_SYSCALL(getuserweight);
        COND_SYSCALL(setuserweight);

- Created kernel/userweight.c with all its content

- Added kernel/Makefile

        obj-y += userweight.o

## Implementing User Weights (commented for now)

- Added include/linux/sched.h to struct task_struct

        /* User weighted round robin */
	unsigned int uwrr_weight;

- Added init/init_task.c

        /* User weighted round robin */
        #define UWRR_DEFAULT_WEIGHT 10

        /* User weighted round robin */
        .uwrr_weight = UWRR_DEFAULT_WEIGHT,
