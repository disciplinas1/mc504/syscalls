#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#define __NR_getuserweight 448
#define __NR_setuserweight 449

int main(void) {
    int uid, weight;

    printf("Choose user id for syscalls:\n");
    scanf("%d", &uid);
    printf("Choose weight for set syscall:\n");
    scanf("%d", &weight);

    int ret_get = syscall(__NR_getuserweight, uid);
    printf("Return from get: %d\n", ret_get);
    if (ret_get == -1) {
        int err = errno;
        printf("error: %d\n", err);
    }

    int ret_set = syscall(__NR_setuserweight, uid, weight);
    printf("Return from set: %d\n", ret_set);
    if (ret_set == -1) {
        int err = errno;
        printf("error: %d\n", err);
    }

    return 0;
}
