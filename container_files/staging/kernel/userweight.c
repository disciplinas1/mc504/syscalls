#include <linux/syscalls.h>
#include "../include/linux/cred.h"

SYSCALL_DEFINE1(getuserweight, int, uid) {
    /* Assert correct uid */
    if (uid < -1 || uid > 65535) {
        return -EINVAL;
    }

    return 0;
}

SYSCALL_DEFINE2(setuserweight, int, uid, int, weight) {
    /* Assert called by root */
    if (!uid_eq(current_uid(), GLOBAL_ROOT_UID)) {
        return -EACCES;
    }

    /* Assert correct uid and correct weight */
    if (uid < -1 || uid > 65535 || weight < 1) {
        return -EINVAL;
    }

    return 0;
}
