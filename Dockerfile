
FROM ubuntu:16.04

# Install dependencies
RUN apt-get -y update

RUN apt-get -y -q install      \
  bc                           \
  flex                         \
  bison                        \
  build-essential              \
  git                          \
  libncurses-dev               \
  libssl-dev                   \
  libelf-dev                   \
  u-boot-tools                 \
  wget                         \
  xz-utils                     \
  qemu-kvm                     \
  iproute2                     \
  python3                      \
  python3-pip                  \
  nano

RUN apt-get -y upgrade

# Install virtme
RUN pip3 install --user git+https://github.com/ezequielgarcia/virtme.git

# Set correct path for virtme command
RUN echo 'export PATH=${PATH}:${HOME}/.local/bin' >> /root/.bashrc
